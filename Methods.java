import java.util.*;

public class Methods {
    public String[][] getTablePosition() {
        /**
         * Aquest mètode s'encarrega d'omplir i retornar una matriu amb totes les posicions d'un tauler d'escacs.
         */
        String[] lletres = new String[]{"a","b","c","d","e","f","g","h"};
        String[][] tauler = new String[8][8];
        for (int i = 7; i>=0; i--)for(int z = 0; z<8; z++)tauler[i][z] = "" + lletres[z] + (i+1);
        return tauler;
    }    
    public static void printTable(String move, String[][] tauler, int nombre, ArrayList<String> blockingPieces){
        /**
         * Aquest mètode imprimeix per pantalla el tauler d'escacs amb la posició del cavall actual i les peces que el bloquegen.
        */         
        Methods main = new Methods();          
        int x = main.getXNumberPosition(move)-1;
        int y = main.getYNumberPosition(move)-1;
        System.out.println("\n");
        for(int i = 7; i>=0; i--){
            for(int h = 0; h<8; h++){
                if(h==x && y == i) System.out.print("  \u2658  ");
                else if (blockingPieces.contains(tauler[i][h])) System.out.print("  \u265F  ");
                else if(((h+1)%2!=0 && (i+1)%2!=0)||((h+1)%2==0 && (i+1)%2==0))System.out.print("  \u25A0  ");
                else System.out.print("  \u25A1  ");
                if(h==7 && i == 3) {
                    if(nombre==0)System.out.print("    Corresponding to the original position. (" + move + ")\n\n");  
                    else if (nombre == 1) System.out.print("    Corresponding to the 1st movement. (" + move + ")\n\n");  
                    else if (nombre == 2) System.out.print("    Corresponding to the 2nd movement. (" + move + ")\n\n");  
                    else System.out.print("    Corresponding to the " + nombre + "th movement. (" + move + ")\n\n");  
                }
                else if(h==7) System.out.println("\n");
            }
        }        
        System.out.println("\n___________________________________________________________________");
    }
    public int numberOfMoves(int a, int b, int c, int d){
        /**
         * Aquest mètode junt amb la matriu arr (declarada al final de tot el codi) calcula el nombre de 
         * moviments possibles que són necessaris per anar d'una posició a una altra.
         */
        int j = 0;
        int x = Math.abs(a-c);
        int y = Math.abs(b-d);
        for(int i = 0; i<36; i++) {
            if(x == arr[i][0] && y == arr[i][1] || x == arr[i][1] && y == arr[i][0]) {
                j = arr[i][2];
                break;
            }
        }            
        return j;   
    }
    public int getXNumberPosition(String lletra){
        String lletres = " abcdefgh";
        int x = new StringBuffer(lletres).indexOf(String.valueOf(lletra.charAt(0))); 
        return x;
    }
    public int getYNumberPosition(String lletra){
        int x = Integer.parseInt(String.valueOf(lletra.charAt(1))); 
        return x;
    }     
    public static int[][] arr = new int[][]{
        { 0, 0, 0} , { 0, 1, 3} ,  {0, 2, 2} ,  {0, 3, 3} , { 0, 4, 2} , {0, 5, 3} , {0, 6, 4} , { 0, 7, 5} , { 1, 1, 2} , { 1, 2, 1} , { 1, 3, 2} ,
        { 1, 4, 3} , { 1, 5, 4} ,  {1, 6, 3} ,  {1, 7, 4} , { 2, 2, 4} , {2, 3, 3} , {2, 4, 2} , { 2, 5, 3} , { 2, 6, 3} , { 2, 7, 5} , { 3, 3, 2} ,
        { 3, 4, 3} , { 3, 5, 4} ,  {3, 6, 3} ,  {3, 7, 4} , { 4, 4, 4} , {4, 5, 3},  {4, 6, 4} , { 4, 7, 5} , { 5, 5, 4} , { 5, 6, 5} , { 5, 7, 4} ,
        { 6, 6, 5} , { 6, 7, 5} ,  {7, 7, 6}};   
}
