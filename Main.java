import java.util.*;
 
public class Main {    
    public static void Main(){
        Scanner sc = new Scanner(System.in);
        Methods main = new Methods();               
        /**
         * Aquí es declaren totes les variables d'arrays i la matriu que necessitarem.
         */        
        ArrayList<String> blockingPieces = new ArrayList<>();  //ArrayList de les peces que bloquegen posicions.
        ArrayList<Integer> array = new ArrayList<>(); //ArrayList que s'usarà per guardar possibles moviments.
        ArrayList<String> moves = new ArrayList<>(); //ArrayList que s'usarà per guardar els moviments "bons".
        ArrayList<Integer> error = new ArrayList<>(Arrays.asList(new Integer[]{100,100,100,100,100,100,100,100}));
        int[] movimentX = new int[]{-2,-1,1,2,-2,-1,1,2}; //Arrays de possibles coordenades
        int[] movimentY = new int[]{1,2,2,1,-1,-2,-2,-1};                
        String[][] matriu = main.getTablePosition();           
        /**
         * Aquí demanem les dades (posició inicial i final, i les posicions de les peces que bloquegen posicions.
         */        
        System.out.print("The knight starts at:  ");
        String inici = sc.nextLine();
        moves.add(inici);
        System.out.print("The knight finishes at:  ");
        String fi = sc.nextLine(),  move = "", block = "";       
        while(!block.equals("xx")){
            System.out.print("\nEnter blocking piece (xx to quit): ");
            block = sc.nextLine();
            if(!block.equals("xx"))  blockingPieces.add(block);            
        }        
        /**
         * Aquí agafem les coordenades de la posició inicial i la final, i calculem el nombre de moviments inicial que s'hauran de dur a terme.
         */
        int x = main.getXNumberPosition(inici);
        int y = main.getYNumberPosition(inici);
        int i = main.getXNumberPosition(fi); 
        int j = main.getYNumberPosition(fi), h = 0;      
        int numOfMoves = main.numberOfMoves(x,y,i,j);   
        int lastX = 0;
        int lastY = 0;
        /**
         * Aquesta és la tasca principal del problema. Mentre el nombre de moviments necessaris per arribar a la posició final no sigui zero es repetirà el cicle.
         * Cada vegada que es repeteixi, el programa comprova, amb l'ajuda dels dos arrays de coordenades, quin dels 8 moviments possibles serà el que necessitarà 
         * menys moviments per arribar al final.
         */   
        while(numOfMoves!=0){
            for(int w = 0; w<8; w++){
                int possibleX = x + movimentX[w]; //Aquí omplim un array amb tots els possibes moviments.
                int possibleY = y + movimentY[w];      
                numOfMoves = main.numberOfMoves(possibleX, possibleY, i,j);
                if(possibleX>=1 && possibleY>=1 && possibleX<=8 && possibleY<=8) move = matriu[possibleY-1][possibleX-1];
                else  move = "impossible";
                if(!move.equals("impossible") && !moves.contains(move) && !blockingPieces.contains(move)) array.add(numOfMoves);
                else  array.add(100);
            }
            if(!array.equals(error)) {            
                int index = (array.indexOf(Collections.min(array)));
                lastX = x;
                lastY = y;
                x = x + movimentX[index]; //Assignem els nous valors de x i y.
                y = y + movimentY[index];
                numOfMoves = main.numberOfMoves(x,y,i,j);
                moves.add(matriu[y-1][x-1]);
                array.clear();
                h++;
            }
            else {
                blockingPieces.add(moves.get(moves.size()-1));    
                moves.remove(moves.size()-1);
                array.clear();
                x = lastX;
                y = lastY;
                h--;
            }
        }  
        System.out.println("\nThe knight’s shortest path is:  " + moves); 
        for(int n = 0; n<(h+1); n++)  main.printTable(moves.get(n), matriu, n, blockingPieces);
        /**
         * Aquí s'imprimeixen per pantalla els moviments necessaris per arribar a la posició final
         */
    }    
}
